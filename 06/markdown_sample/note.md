# Tâioânōe ê bīlâi

* Tâioânlâng kóng Tâioânōe
  * Bô kóng lín tau ê tāichì
* Ū kóng ū pópì

[Mandrill (Wiki)](https://en.wikipedia.org/wiki/Mandrill)

![](./pics/mandrill.png)

---

Iáⁿphìⁿ ū kóng tio̍h ê bāngliān:

* [Markdown Cheat Sheet](https://www.markdownguide.org/cheat-sheet/)
* [Markdown Viewer Webext](https://addons.mozilla.org/en-US/firefox/addon/markdown-viewer-webext/)
  * [Support for local files on Linux](https://github.com/KeithLRobertson/markdown-viewer#support-for-local-files-on-linux)
